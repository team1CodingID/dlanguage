import React, { useState } from "react";
import './Register.css';
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

export default function Register(){
    const [customerName,setName] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [copassword,setcoPassword] = useState("");
    const navigate = useNavigate();
    
    const confirmPassword = (e) => {
        
        if(e.target.value != password){
            setcoPassword("Password Tidak Sama");
        } else{
            setcoPassword("");
        }
    }

    const handleRegister = () => {
        axios.post(`${process.env.REACT_APP_API_URL}/User/CreateUser`, {customerName,email, password, role: 'User'});
        navigate("/registerSuccess")
    }

    return (
        <div className="containerLogin">
            <div className="containerTitle">
                <div className="title-1">Lets Join <b className="title-2">D'Language</b></div>
            </div>
            <div className="word">Please register first</div>
            <form className="form">
                <input type="text" className="name" placeholder="Name" name="name" value={customerName} onChange={(e) => setName(e.target.value)}/>
                <input type="email" className="email" placeholder="Email" name="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                <input type="password" className="password" placeholder="Password" name="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                <input type="password" className="co-password" placeholder="Confirm Password" name="co-password"  onChange={confirmPassword}/>
                <div style={{color:"red"}}>{copassword}</div>
                <div className="containerSubmit">
                <button className="submitSignUp" onClick={handleRegister}>Sign Up</button>
                </div>
            </form>
            <div className="wordLogin">Have account?  
                <Link to="/login" className="linkLogin">
                    &nbsp; Login here
                </Link>
            </div>
        </div>
    );
}