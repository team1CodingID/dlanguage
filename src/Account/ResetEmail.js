import React, { useState } from "react";
import './Reset-Email.css';
import { Link } from "react-router-dom";
import axios from "axios";

export default function ResetEmail(){
    const [emailReset, setEmailReset] = useState("");
    const [emailSent, setEmailSent] = useState(false);
    const [error, setError] = useState("");

    const handleResetEmail = (e) => {
        e.preventDefault(); // Prevent the default form submission behavior
        console.log(emailReset);
        axios.post(`${process.env.REACT_APP_API_URL}/User/ForgetPassword`, emailReset, { headers: {'Content-Type': 'application/json', 'Accept': 'application/json'} })
            .then(response => {
                console.log('Email sent successfully:', response.data);
                setEmailSent(true);
                setError("");
            })
            .catch(function (error) {
                if (error.response) {
                  console.log('Server responded with status code:', error.response.status);
                  console.log('Response data:', error.response.data);
                } else if (error.request) {
                  console.log('No response received:', error.request);
                } else {
                  console.log('Error creating request:', error.message);
                }
              });
    }
    
    return (
        <div className="containerReset">
            <div className="titleReset">Reset Password</div>
            <div className="word">Please enter your email address</div>
            <form className="form" onSubmit={handleResetEmail}>
                <input type="email" className="email" placeholder="Email" name="email" value={emailReset} onChange={(e) => setEmailReset(e.target.value)}/>
                <div className="containerSubmit">
                    <Link to="/login">
                        <button className="submitCancel">Cancel</button>
                    </Link>
                    <button className="submitConfirm" type="submit">Confirm</button>
                </div>
            </form>
        </div>
    );
}