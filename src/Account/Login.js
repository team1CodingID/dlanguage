import React, { useState } from "react";
import './Login.css';
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import useAuth from "../hook/useAuth";

export default function Login({ onLogin }){
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [ payload, setPayload ] = useState(null);
    const navigate = useNavigate();

    const { login } = useAuth();

    const handelLogin = async (e) => {
        e.preventDefault();
        try {
          const response = await axios.post(`${process.env.REACT_APP_API_URL}/User/login`, { email, password });
          const responseData = response.data;
          console.log(responseData);
          setPayload(responseData);
      
          if (responseData.userRole === 'Admin') {
            console.log("masuk Admin");
            login(responseData);
            navigate('/Admin');
          } else {
            console.log("masuk User");
            login(responseData);
            navigate('/');
          }
        } catch (error) {
          alert(error.response.data);
          navigate('/login');
        }
      }
    
    return (
        <div className="containerLogin">
            <div className="greet">Welcome   Back!</div>
            <div className="word">Please login first</div>
            <form className="form" onSubmit={handelLogin}>
                <input type="email" className="email" placeholder="Email" name="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                <input type="password" className="password" placeholder="Password" name="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                <div className="word">Forgot Password? 
                    <Link to="/resetEmail" className="linkForgot">
                        &nbsp;Click Here
                    </Link>
                </div>
                <div className="containerSubmit">
                    <button className="submitLogin" type="submit">Login</button>
                </div>
            </form>
            <div className="wordSignUp">Dont have account? 
                <Link to="/register" className="linkSignUp">
                    &nbsp;Sign Up here
                </Link>
            </div>
        </div>
    );
}