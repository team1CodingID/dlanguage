import React from "react";
import './RegisterSuccess.css';
import { Link } from "react-router-dom";

export default function RegisterSuccess(){
    return (
        <div className="containerSuccess">
            <img src={process.env.PUBLIC_URL + "EmailConf.svg"} alt="Success Sign Up Logo" className="successImage"/>
            <div className="successWord">Email Confirmation Success</div>
            <div>Thanks for confirmation your email. Please login first</div>
            <Link to="/login">
                <button className="btnBackToLogin">Login Here</button>
            </Link>
        </div>
    );
}