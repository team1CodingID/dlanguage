import React, { useEffect, useState } from "react";
import './ResetPassword.css';
import { Link, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

export default function ResetEmail(){
    const [password,setPassword] = useState("");
    const [copassword,setcoPassword] = useState("");
    const [cond,setCond] = useState(true);
    const navigate = useNavigate();
    const emailParams = useParams();
    
    const submitForm = (e) =>{
        e.preventDefault(); // Prevent the default form submission behavior
        console.log("masuk");
        if(password === copassword){
            setCond(true);
        } else {
            setCond(false);
        }
        
        if (cond) {
            axios.post(`${process.env.REACT_APP_API_URL}/User/ResetPassword`, {Password: password, ConfirmPassword : copassword, Email : emailParams.email}, { headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}}).then(response => {
                console.log('Email sent successfully:', response.data);
            }).catch(function (error) {
                if (error.response) {
                  console.log('Server responded with status code:', error.response.status);
                  console.log('Response data:', error.response.data);
                } else if (error.request) {
                  console.log('No response received:', error.request);
                } else {
                  console.log('Error creating request:', error.message);
                }
              });
              navigate('/login');
        } else {
            alert("Password Belum sama");
        }
    }

    return (
        <div className="containerReset">
            <div className="titleReset">Create Password</div>
            <form className="form" onSubmit={submitForm}>
                <input type="password" className="password" placeholder="Password" name="password" onChange={(e) => {setPassword(e.target.value); console.log(password)}}/>
                <input type="password" className="co-password" placeholder="Confirm Password" name="co-password" onChange={e => { setcoPassword(e.target.value) ; console.log(copassword) }}/>
                <div className="containerSubmit">
                    <Link to="/login">
                        <button className="submitCancel">Cancel</button>
                    </Link>
                    <button type="submit" className="submitSubmit">Submit</button>
                </div>
            </form>
        </div>
    );
}