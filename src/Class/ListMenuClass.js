import React, { useState, useEffect } from "react";
import Footer from "../component/Footer.js";
import Class from "../component/Class.js";
import { Box, Card, CardMedia, Grid, Typography, CardActionArea } from "@mui/material";
import CardContent from "@mui/material/CardContent";
import { useLocation, useParams } from "react-router-dom";
import axios from "axios";

export default function ListMenuClass() {
  const location = useLocation();
  const { courseId } = location.state; // Corrected destructuring

  const [course, setCourse] = useState([]);
  const [listClass, setListClass] = useState([]);
  const [loading, setLoading] = useState(true);

  console.log(courseId);

  useEffect(() => {
    if (courseId) {
      // Corrected the API request by adding courseId as a query parameter
      axios
        .get(`${process.env.REACT_APP_API_URL}/course/GetById?courseId=${courseId}`)
        .then((res) => {
          console.log(res.data);
          setCourse(res.data);
          setLoading(false);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
          setLoading(false);
        });

      axios
        .get(`${process.env.REACT_APP_API_URL}/class/GetByCategory?courseId=${courseId}`)
        .then((res) => {
          setListClass(res.data);
          console.log(res.data);
        })
        .catch(e => {
          console.error("Error fetching data:", e);
        });
    }
  }, [courseId]);

  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${process.env.PUBLIC_URL + "/MenuClass1.svg"})`,
          backgroundSize: "100%",
          backgroundRepeat: "no-repeat",
          height: 350,
        }}
      ></div>
      <Box sx={{ mx: 10, mb: 40 }} mt={{ lg: 10, xs: 0 }}>
        {loading ? (
          <p>Loading...</p>
        ) : (
          <>
            <Typography sx={{ fontSize: 24, fontWeight: 600, color: "#226957" }}>
              {course.courseName}
            </Typography>
            <Typography sx={{ fontSize: 16, fontWeight: 200, color: "#226957" }}>
              {course.description}
            </Typography>
          </>
        )}
      </Box>
      <Typography sx={{ textAlign: "center", fontSize: 24, fontWeight: 600, color: "#226957" }}>
        Class you might like
      </Typography>
      <Grid container spacing={5} sx={{ width: "100%", mx: 20, mt: 10 }} xs={{ mx: 0 }}>
        {!loading &&
          listClass.map((item) => (
            <Grid item xs={12} md={6} lg={3} width={{ xs: 250 }} key={item.id}>
              <Card sx={{ height: 400, borderRadius: 10 }}>
                <CardActionArea>
                  <CardMedia component="img" height="235" image={process.env.PUBLIC_URL+'/class/'+item.imgPath} alt={item.title} />
                  <CardContent sx={{ height: 150 }}>
                    <Typography gutterBottom variant="body" component="div">
                      {item.courseName}
                    </Typography>
                    <Typography sx={{ fontSize: 18, fontWeight: 600 }}>{item.title}</Typography>
                    <Typography
                      sx={{
                        color: "#226957",
                        fontSize: 18,
                        fontWeight: 600,
                        my: "auto",
                        left: 2,
                        bottom: 2,
                      }}
                    >
                      {item.price}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            
          ))}
      </Grid>
      <Footer />
    </div>
  );
}
