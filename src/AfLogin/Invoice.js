import * as React from 'react';
import InvoiceTable from '../component/InvoiceTable';
import Footer from '../component/Footer';
import { Typography } from '@mui/material';
import useAuth from '../hook/useAuth';
import { useNavigate } from 'react-router-dom';

export default function Invoice() {
  const { payload } = useAuth();
  const navigate = useNavigate();

  return (
    <>
      {payload && payload.userRole === "User" ? (
          <div>
          <Typography variant='body' sx={{ mx: 12, my: 5 }}>
              Home {'>'} Invoice
          </Typography>
          <Typography variant='h4' sx={{ mx: 12, my: 5 }}>
              Menu Invoice
          </Typography>
          <InvoiceTable/>
          <Footer />
      </div>
      ) : (
          <>
              {navigate('/')}
          </>
      )}
    </>
  );
}
