import * as React from 'react';
import Footer from '../component/Footer';
import { Grid, Typography } from '@mui/material';
import DetailInvoiceTable from '../component/DetailInvoiceTable';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import { useState } from 'react';

export default function DetailInvoice() {
  const location = useLocation();
  const { orderId } = location.state;
  const [detailsGeneral, setDetailsGeneral] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/order/detailInvoice?orderId=${orderId}`)
    .then(res => {
      setDetailsGeneral(res.data);
      console.log("Success General Detail: ");
    })
    .catch(error => {
      console.error("Error General Detail: ", error);
    })
  }, [orderId])
  
  return (
    <div>
        <Typography variant='body' sx={{ mx: 12, my: 5 }}>
            Home {'>'} Invoice {'>'} Details Invoice
        </Typography>
        <Typography variant='h4' sx={{ mx: 12, my: 5 }}>
            Details Invoice
        </Typography>
        <Grid container sx={{ px:12, my: 2 }}>
          <Grid item lg={2}>
            No. Invoice:
          </Grid>
          <Grid item lg={10}>
            {detailsGeneral[0]?.invoiceCode}
          </Grid>
        </Grid>
        <Grid container sx={{ px:12, my: 2 }}>
          <Grid item lg={2}>
            Date:
          </Grid>
          <Grid item lg={8}>
            {detailsGeneral[0]?.orderDate}
          </Grid>
          <Grid item lg={2}>
            Total Price &emsp; IDR {detailsGeneral[0]?.totalPrice}
          </Grid>
        </Grid>
        <DetailInvoiceTable />
        <Footer />
    </div>
  );
}
