import { Box, Button, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import PaymentMethod from "../component/PaymentMethod";
import useAuth from "../hook/useAuth";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function Cart(){
    const {payload} = useAuth();
    const [cart, setCart] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}/detailOrder?customerId=${payload.customerId}`)
        .then(res => {setCart(res.data); console.log(res.data);})
        .catch(error => console.error("Error fetching data:", error));
    },[payload.customerId]);

    const deleteItem = (detailId, event) => {
        event.preventDefault();
        axios.delete(`${process.env.REACT_APP_API_URL}/detailOrder?detailId=${detailId}`)
        .then(() => {console.log("Success Deleted"); window.location.reload(true)})
        .catch(error => console.error(error));
    }

    return(
        <>
            {payload && payload.userRole === "User" ? (
                <div style={{ position: "relative", maxWidth: "inherit" }}>
                    {cart.map(item => {
                        return (
                            <div>
                                <Grid container sx={{ px: 10 }}>
                                    <Grid item lg={2}>
                                        <CardMedia 
                                        sx={{ height: 135, borderRadius: 0, margin: 2, border: "none"}}
                                        image={`data:image/png;base64,${item.imageData}`}
                                        />
                                    </Grid>
                                    <Grid item lg={7}>
                                        <CardContent>
                                            <Typography variant="body2" color="text.secondary">
                                                {item.courseName}
                                            </Typography>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {item.classTitle}
                                            </Typography>
                                            <Typography gutterBottom variant="h5" component="div">
                                                IDR {item.price}
                                            </Typography>
                                        </CardContent>
                                    </Grid>
                                    <Grid item lg={3}>
                                        <CardActions sx={{ height: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
                                            <Button onClick={(e) => deleteItem(item.detailId, e)} sx={{ color: "red", borderRadius: 5, '&:hover': {backgroundColor: "#EA9E1F" }}}> <DeleteForeverIcon sx={{ width: 50, height: 40 }} /> </Button>
                                        </CardActions>
                                    </Grid>
                                </Grid>
                                <hr style={{ width: "90%" }}/> 
                            </div>
                        );
                    })}
                    <Box sx={{ position: "fixed", bottom: 0, width: "100%", backgroundColor: "white" }}>
                        <hr/>
                        <Grid container sx={{ px: {sm:5, lg:10}, py: 2 }}>
                            <Grid item lg={10} xs={6} md={8}>
                                <Typography variant="body2" color="text.secondary" sx={{ display: "flex", lineHeight: 3, verticalAlign: "center" }}>
                                    Total Price
                                    <Typography gutterBottom variant="h5" component="div" sx={{ ml: 3, lineHeight: 2, verticalAlign: "center", fontWeight: 700 }}>
                                        IDR {cart[0]?.totalPrice}
                                    </Typography>
                                </Typography>
                            </Grid>
                            <Grid item lg={2} xs={6} md={4}>
                                <PaymentMethod/>
                            </Grid>
                        </Grid>
                    </Box>
                </div>
            ) : (
                <>
                    {navigate('/')}
                </>
            )}
        </>
    );
}