import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, FormControl, Grid, InputLabel, MenuItem, Select, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useParams, useLocation, useNavigate } from "react-router-dom";
import Footer from "../component/Footer";
import axios from "axios";
import useAuth from "../hook/useAuth";

export default function DetailCourse(){ 
    const location = useLocation();
    const { classId, courseId, imageData } = location.state;
    const { payload } = useAuth();

    const [ scheduleDate, setScheduleDate ] = useState("");
    const [cls, setCls] = useState([]);
    const [oneCls, setOneCls] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        axios
        .get(`${process.env.REACT_APP_API_URL}/class/GetById?classId=${classId}`)
        .then((res) => {
          console.log(res.data);
          setOneCls(res.data);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });

        axios
        .get(`${process.env.REACT_APP_API_URL}/class/GetByCategory?courseId=${courseId}`)
        .then((res) => {
          setCls(res.data);
          console.log(res.data);
        })
        .catch(e => {
          console.error("Error fetching data:", e);
        });
    }, [classId]);

    const addToCart = () => {
        if(scheduleDate === ""){
            alert('Wajib Memilih Schedule Date');
        } else {
            console.log("masuk");
            console.log(scheduleDate);
            axios.put(`${process.env.REACT_APP_API_URL}/detailOrder?classId=${classId}&customerId=${payload.customerId}`,
                JSON.stringify(scheduleDate), // Convert the date to a JSON string
                {
                    headers: {
                    'accept': '*/*',
                    'Content-Type': 'application/json'
                    }
                }
            )
            .then(res => {
                console.log(res);
                navigate('/cart');
            })
            .catch(error => {console.error("Error : " + error.response.data);});
        }
    } 

    return(
        <div>
            <Grid container>
                <Grid item lg={4} xs={12}>
                    <CardMedia 
                    sx={{ height: 265, borderRadius: 0, margin: 2, border: "none"}}
                    image={`data:image/png;base64,${imageData}`}
                    />
                </Grid>
                <Grid item lg={8}>
                <CardContent>
                    <Typography variant="body2" color="text.secondary">
                        {oneCls.courseName}
                    </Typography>
                    <Typography gutterBottom variant="h5" component="div">
                        {oneCls.title}
                    </Typography>
                    <Typography gutterBottom variant="h5" component="div">
                        IDR {oneCls.price}
                    </Typography>
                    <div sx={{width: 330}}>
                        <InputLabel id="demo-simple-select-label">Schedule Date</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={scheduleDate}
                            label="Age"
                            onChange={(e) => setScheduleDate(e.target.value)}
                        >
                            <MenuItem value={"2023-10-25"}>Monday, 25 Oktober 2023</MenuItem>
                            <MenuItem value={"2023-10-26"}>Tuesday, 26 Oktober 2023</MenuItem>
                            <MenuItem value={"2023-10-27"}>Wednesday, 27 Oktober 2023</MenuItem>
                            <MenuItem value={"2023-10-28"}>Thursday, 28 Oktober 2023</MenuItem>
                            <MenuItem value={"2023-10-29"}>Friday, 29 Oktober 2023</MenuItem>
                            <MenuItem value={"2023-10-30"}>Saturday, 30 Oktober 2023</MenuItem>
                        </Select>
                    </div>
                </CardContent>
                <CardActions>
                    <Button onClick={addToCart} sx={{backgroundColor: "#EA9E1F", color: "white", width: {xs: 180, md: 235}, height: 40, borderRadius: 5, '&:hover': {backgroundColor: "#EA9E1F"}}}>Add to Cart</Button>
                    <Button onClick={addToCart} sx={{backgroundColor: "#226957", color: "white", width: {xs: 180, md: 235}, height: 40, borderRadius: 5, '&:hover': {backgroundColor: "#226957"}}}>Buy Now</Button>
                </CardActions>
                </Grid>
            </Grid>
            <Box sx={{mx: 3}}>
                <Typography gutterBottom variant="h5" component="div">
                    Description
                </Typography>
                <Typography variant="body" color="text.secondary">
                    {oneCls.description}
                </Typography>
            </Box>
            <Typography sx={{ textAlign: "center", fontSize: 24, fontWeight: 600, color: "#226957", marginTop: 30 }}>
                Another Class For You
            </Typography>
            <Grid container spacing={5} sx={{ width: "100%", mx: 'auto', my: 5 }} xs={{ mx: 0 }}>
                {cls.map(course => (
                    <Grid item xs={12} md={6} lg={3} width={{ xs: 250 }} key={course.classId}>
                    <Card sx={{ height: 400, borderRadius: 10 }}>
                        <CardActionArea>
                        <CardMedia component="img" height="235" image={`data:image/png;base64,${course.imageData}`} alt={course.title} />
                        <CardContent sx={{ height: 150 }}>
                            <Typography gutterBottom variant="body" component="div">
                            {course.courseName}
                            </Typography>
                            <Typography sx={{ fontSize: 18, fontWeight: 600 }}>{course.title}</Typography>
                            <Typography
                            sx={{
                                color: "#226957",
                                fontSize: 18,
                                fontWeight: 600,
                                my: "auto",
                                left: 2,
                                bottom: 2,
                            }}
                            >
                                IDR {course.price}
                            </Typography>
                        </CardContent>
                        </CardActionArea>
                    </Card>
                    </Grid>
                ))}
            </Grid>
            <Footer />
        </div>
    );
}
