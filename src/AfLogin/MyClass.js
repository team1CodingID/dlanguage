import { CardContent, CardMedia, Grid, Typography } from "@mui/material";
import React, { useState } from "react";
import useAuth from "../hook/useAuth";
import { useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function MyClass(){
    const { payload } = useAuth();
    const [myClass, setMyClass] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}/order/myClass?customerId=${payload.customerId}`)
        .then(res => {
            setMyClass(res.data)
            console.log("Succes Get Data : ", res);
        })
        .catch(error => {
            console.error("Error With Response : ", error);
        })
    },[payload.customerId]);

    return(
        <>
            {payload && payload.userRole === "User" ? (
                <div style={{ position: "relative", maxWidth: "inherit" }}>
                    {myClass.map((clas) => {
                        return (
                            <div>
                                <Grid container sx={{ px: 10 }}>
                                    <Grid item lg={2} sm={4} xs={12}>
                                        <CardMedia 
                                        sx={{ height: 135, borderRadius: 0, margin: 2, border: "none"}}
                                        image={`data:image/png;base64,${clas.imageData}`} 
                                        />
                                    </Grid>
                                    <Grid item lg={7} sm={8} xs={12} sx={{ my: "auto" }}>
                                        <CardContent>
                                            <Typography variant="body2" color="text.secondary">
                                                {clas.courseName}
                                            </Typography>
                                            <Typography gutterBottom variant="h6" component="div" sx={{ fontWeight: 700 }}>
                                                {clas.classTitle}
                                            </Typography>
                                            <Typography gutterBottom variant="h6" component="div" sx={{ color: "#EA9E1F" }}>
                                                Schedule : {clas.scheduleDate}
                                            </Typography>
                                        </CardContent>
                                    </Grid>
                                </Grid>
                                <hr style={{ width: "90%" }}/>
                            </div>
                        );
                    })}
                </div>
            ) : (
                <>
                    {navigate('/')}
                </>
            )}
        </>
    );
}