import React from "react";
import { Link } from "react-router-dom";
import { Button, Typography } from "@mui/material";

export default function PaymentSuccess(){
    return (
        <div style={{ display: "block",
            width: 620,
            margin: "50px auto",
            fontSize: 16,
            textAlign: "center", }}>
            <img src={process.env.PUBLIC_URL + "EmailConf.svg"} alt="Success Sign Up Logo" className="successImage"/>
            <Typography sx={{ fontSize: 24, fontWeight: 500, color: "#226957" }}>
                Purchase Successfully
            </Typography>
            <Typography sx={{ fontSize: 16, fontWeight: 400, color: "#226957", mb: 5 }}>
                Thanks to buy a course! See u in the class
            </Typography>
            <Link to="/">
                <Button sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 1, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Back To Home</Button>
            </Link>
            <Link to="/invoice">
                <Button sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 1, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Open Invoice</Button>
            </Link>
        </div>
    );
}