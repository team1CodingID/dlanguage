import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import { useState } from 'react';
import { useEffect } from 'react';
import useAuth from '../hook/useAuth';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import CustomerAdd from './AdminAdd/CustomerAdd';
import CustomerEdit from './AdminEdit/CustomerEdit';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function UserTableAdmin() {
  const [order, setOrder] = useState([]);
  const { payload } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/AdminCustomer`)
        .then(res => {
          setOrder(res.data);
          console.log("Data Accepted");
        })
        .catch(error => {
          console.error("Error fetching data:", error);
        });
  },[]);

  const details = (orderId) => {
    navigate('/invoice/Detail', {state : {orderId: orderId}});
  }

  const active = (customerId) => {
    axios.put(`${process.env.REACT_APP_API_URL}/AdminCustomer/Active?customerId=${customerId}`)
    .then(res => {
      console.log("Succes With Response : ", res);
      window.location.reload(true);
    })
    .catch(error => {
      console.error("Error with response : ", error)
    });
  }
  
  return (
    <div>
      {payload && payload.userRole === "Admin" ? (
        <div>
          <CustomerAdd />
            <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
              <Table sx={{ maxWidth: 1400, mx: "auto" }} aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell align='center'>No</StyledTableCell>
                    <StyledTableCell align='center'>Customer Id</StyledTableCell>
                    <StyledTableCell align='center'>Customer Name</StyledTableCell>
                    <StyledTableCell align='center'>Email</StyledTableCell>
                    <StyledTableCell align='center'>Role</StyledTableCell>
                    <StyledTableCell align='center'>Action</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {order.map((row, index) => (
                    <StyledTableRow key={row.classId}>
                      <StyledTableCell component="th" scope="row" align='center'>
                        {index+1}
                      </StyledTableCell>
                      <StyledTableCell align='center'>{row.customerId}</StyledTableCell>
                      <StyledTableCell align='center'>{row.customerName}</StyledTableCell>
                      <StyledTableCell align='center'>{row.email}</StyledTableCell>
                      <StyledTableCell align='center'>{row.role}</StyledTableCell>
                      <StyledTableCell align='center'>
                        <CustomerEdit order={row}/>
                        <Button onClick={() => active(row.customerId)} sx={{backgroundColor: "#EA9E1F", color: "white", width: 150, height: 30, mt: 2, borderRadius: 2, '&:hover': {backgroundColor: "#EA9E1F"}}}>Active/Inactive</Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
        </div>
      ) : (
        <>
          {navigate('/')}
        </>
      )}
    </div>
  );
}
