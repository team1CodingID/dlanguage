import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, MenuItem, Select, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Customer(props) {
  const { onClose, open, order } = props;
  const [customerName, setCustomerName] = useState(order.customerName);
  const [email, setEmail] = useState(order.email);
  const [password, setPassword] = useState("");
  const [role, setRole] = useState(order.role);

  const create = (e) => {
    axios.put(`${process.env.REACT_APP_API_URL}/AdminCustomer?customerId=${order.customerId}`, { customerName: customerName, email: email, password: password, role: role })
    .then(res => {
      console.log("Success with Response : ", res);
      onClose();
    })
    .catch(error => {
      console.error("Error With Response : ", error.response);
    })
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={create} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Category</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
            <TextField onChange={(e) => setCustomerName(e.target.value)} value={customerName} id="standard-basic" label="Customer Name" variant="standard" sx={{ width:300 }}/>
          </List>
          <List sx={{ maxWidth: 300 ,mx: "auto"}}>
            <TextField onChange={(e) => setEmail(e.target.value)} value={email} id="standard-basic" label="Email" variant="standard" sx={{ width:300 }}/>
          </List>
          <List sx={{ maxWidth: 300 ,mx: "auto"}}>
            <TextField onChange={(e) => setPassword(e.target.value)} id="standard-basic" label="Password" variant="standard" type='password' sx={{ width:300 }}/>
          </List>
          <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={role}
                label="Role"
                onChange={(e) => setRole(e.target.value)}
                sx={{ width: 300 }}
            >
                <MenuItem value="User">User</MenuItem>
                <MenuItem value="Admin">Admin</MenuItem>
            </Select>
          </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Cancel</Button>
        <Button onClick={create} sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Edit</Button>
      </Box>
    </Dialog>
  );
}

Customer.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function CustomerEdit(props) {
  const { order } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{backgroundColor: "blue", color: "white", width: 150, height: 30, mt: 2, borderRadius: 2, '&:hover': {backgroundColor: "blue"}}}>Edit</Button>
      <Customer
        open={open}
        onClose={handleClose}
        order={order}
      />
    </div>
  );
}