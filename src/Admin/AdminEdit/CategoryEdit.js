import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Category(props) {
  const { onClose, open, order } = props;
  const [courseName, setCourseName] = useState(order.courseName);
  const [description, setDesc] = useState(order.description);
  const [coursePath, setCoursePath] = useState(order.coursePath);

  console.log(courseName, description, coursePath);

  const create = (e) => {
    const formData = new FormData();

    formData.append('courseName', courseName);
    formData.append('description', description);
    formData.append('coursePath', coursePath);
    
    axios.put(`${process.env.REACT_APP_API_URL}/AdminCourse/Edit?courseId=${order.courseId}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'accept': '*/*',
      }
    })
    .then(res => {
      console.log("Success with Response : ", res);
      onClose();
    })
    .catch(error => {
      console.error("Error With Response : ", error.response);
    })
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={create} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Category</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setCourseName(e.target.value)} value={courseName} id="standard-basic" label="Course Name" variant="standard" sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setDesc(e.target.value)} value={description} id="standard-basic" label="Description" variant="standard" sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setCoursePath(e.target.value)} value={coursePath} id="standard-basic" label="Course Path" variant="standard" sx={{ width:300 }}/>
        </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Cancel</Button>
        <Button onClick={create} sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Edit</Button>
      </Box>
    </Dialog>
  );
}

Category.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function CategoryEdit(props) {
  const { order } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{backgroundColor: "blue", color: "white", width: 150, height: 30, mt: 2, borderRadius: 2, '&:hover': {backgroundColor: "blue"}}}>Edit</Button>
      <Category
        open={open}
        onClose={handleClose}
        order={order}
      />
    </div>
  );
}