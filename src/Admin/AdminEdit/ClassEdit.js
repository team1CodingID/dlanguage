import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, MenuItem, Select, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Clas(props) {
  const { onClose, open, order, clas } = props;
  const [courseId, setCourseId] = useState(order.courseId);
  const [title, setTitle] = useState(order.title);
  const [price, setPrice] = useState(order.price);
  const [imgPath, setImgPath] = useState(order.imgPath);
  const [description, setDesc] = useState(order.description);

  const create = (e) => {
    axios.put(`${process.env.REACT_APP_API_URL}/AdminClass?classId=${order.classId}`, { courseId: courseId, title: title, price: price, imgPath: imgPath, description: description })
    .then(res => {
      console.log("Success with Response : ", res);
      onClose();
    })
    .catch(error => {
      console.error("Error With Response : ", error.response);
    })
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={create} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Category</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={courseId}
              label="Category"
              onChange={(e) => setCourseId(e.target.value)}
              sx={{ width: 300 }}
          >
              {clas.map((item) => {
                return (
                  <MenuItem value={item.courseId}>{item.courseName}</MenuItem>
                );
              })}
          </Select>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setTitle(e.target.value)} value={title} id="standard-basic" label="Title" variant="standard" sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setPrice(e.target.value)} value={price} id="standard-basic" label="Price" variant="standard" type='number' sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setImgPath(e.target.value)} value={imgPath} id="standard-basic" label="Image Path" variant="standard" sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setDesc(e.target.value)} value={description} id="standard-basic" label="Description" variant="standard" sx={{ width:300 }}/>
        </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Cancel</Button>
        <Button onClick={create} sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Edit</Button>
      </Box>
    </Dialog>
  );
}

Clas.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function ClassEdit(props) {
  const { order } = props;
  const [open, setOpen] = React.useState(false);
  const [clas, setCls] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/AdminCourse`)
    .then(res => {
      console.log("Success with Response : ", res);
      setCls(res.data)
    })
    .catch(error => {
      console.error("Error With Response : ", error.response);
    })
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{backgroundColor: "blue", color: "white", width: 150, height: 30, mt: 2, borderRadius: 2, '&:hover': {backgroundColor: "blue"}}}>Edit</Button>
      <Clas
        open={open}
        onClose={handleClose}
        order={order}
        clas={clas}
      />
    </div>
  );
}