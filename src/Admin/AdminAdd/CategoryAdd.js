import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';

function Category(props) {
  const { onClose, open } = props;
  const [courseName, setCourseName] = useState("");
  const [description, setDesc] = useState("");
  const [coursePath, setCoursePath] = useState("");
  const [image, setImage] = useState(null);

  const create = () => {
    const formData = new FormData();

    formData.append('courseName', courseName);
    formData.append('description', description);
    formData.append('coursePath', coursePath);
    formData.append('active', false);
    formData.append('ImageFile', image);
    formData.append('ImageData', 'null');

    axios
      .post(`${process.env.REACT_APP_API_URL}/course`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          'accept': '*/*',
        },
      })
      .then((res) => {
        console.log("Success with Response: ", res);
        onClose();
      })
      .catch((error) => {
        console.error("Error With Response: ", error.response);
      });
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={create} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Category</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <TextField onChange={(e) => setCourseName(e.target.value)} id="standard-basic" label="Course Name" variant="standard" sx={{ width: 300 }} />
        </List>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <TextField onChange={(e) => setDesc(e.target.value)} id="standard-basic" label="Description" variant="standard" sx={{ width: 300 }} />
        </List>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <TextField onChange={(e) => setCoursePath(e.target.value)} id="standard-basic" label="Course Path" variant="standard" sx={{ width: 300 }} />
        </List>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <input type='file' onChange={(e) => setImage(e.target.files[0])} />
        </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{ backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': { backgroundColor: "#EA9E1F" } }}>Cancel</Button>
        <Button onClick={create} sx={{ backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': { backgroundColor: "#226957" } }}>Create</Button>
      </Box>
    </Dialog>
  );
}

Category.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function CategoryAdd() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{ backgroundColor: "#226957", color: "white", width: 135, height: 40, borderRadius: 3, mx: 12, my: 2, '&:hover': { backgroundColor: "#226957" } }}>Add</Button>
      <Category
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}
