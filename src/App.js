import './App.css';
import Navbar from './component/Navbar.js';
import Login from './Account/Login.js';
import Register from './Account/Register.js';
import ResetEmail from './Account/ResetEmail.js';
import ResetPassword from './Account/ResetPassword.js';
import RegisterSuccess from './Account/RegisterSuccess.js';
import LandingPage from './Landing Page/LandingPage.js';
import { Routes, Route } from "react-router-dom";
import ListMenuClass from './Class/ListMenuClass';
import DetailCourse from "./AfLogin/DetailCourse.js";
import Cart from "./AfLogin/Cart";
import Invoice from "./AfLogin/Invoice";
import MyClass from "./AfLogin/MyClass";
import PaymentSuccess from "./AfLogin/PaymentSuccess";
import { AuthProvider } from "./context/AuthContext";
import DetailInvoice from './AfLogin/DetailInvoice';
import NavbarAdmin from './component/NavbarAdmin';
import InvoiceTableAdmin from './Admin/InvoiceTableAdmin';
import CategoryTableAdmin from './Admin/CategoryTableAdmin';
import ClassTableAdmin from './Admin/ClassTableAdmin';
import MethodTableAdmin from './Admin/MethodTableAdmin';
import UserTableAdmin from './Admin/UserTableAdmin';

function App() {
  return (
    <AuthProvider>
      <Routes>
        <Route path='/' element={<Navbar/>}>
          <Route path='/' element={<LandingPage/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/resetEmail' element={<ResetEmail/>}/>
          <Route path='/resetPassword/:email' element={<ResetPassword/>}/>
          <Route path='/registerSuccess' element={<RegisterSuccess/>}/>
          
          <Route path='/listClass/:courseName' element={<ListMenuClass/>}/>
          <Route path='/detailCourse/:course' element={<DetailCourse/>}/>
          <Route path='/cart' element={<Cart />}/>
          <Route path='/myClass' element={<MyClass/>}/>
          <Route path='/invoice' element={<Invoice/>}/>
          <Route path='/invoice/Detail' element={<DetailInvoice/>}/>
          <Route path='/PaymentSuccess' element={<PaymentSuccess />}/>
        </Route>

        <Route path='/Admin' element={<NavbarAdmin/>}>
          <Route path='/Admin' element={<CategoryTableAdmin/>}/>
          <Route path='/Admin/course' element={<ClassTableAdmin />}/>
          <Route path='/Admin/user' element={<UserTableAdmin/>}/>
          <Route path='/Admin/method' element={<MethodTableAdmin/>}/>
          <Route path='/Admin/invoice' element={<InvoiceTableAdmin/>}/>
          <Route path='/Admin/Detail' element={<DetailInvoice/>}/>
        </Route>
      </Routes>
    </AuthProvider>
  );
}

export default App;
