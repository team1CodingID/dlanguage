import '../component/Navbar.css';
import {Outlet, Link, useNavigate } from "react-router-dom";
import PersonIcon from '@mui/icons-material/Person';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import LogoutIcon from '@mui/icons-material/Logout';
import Button from '@mui/material/Button';  
import useAuth from "../hook/useAuth";
import Cookies from 'js-cookie';

export default function Navbar(){
    const { isLoggedIn, payload } = useAuth();
    const navigate = useNavigate();

    const logout = () => {
        Cookies.remove('payload');
        navigate('/');
        window.location.reload(true)
    }
    
    return(
        <div>
            <div className="container">
            <img className="logo" src={process.env.PUBLIC_URL + "/Logo.svg"} alt="Logo DLanguage"/>
            {isLoggedIn && payload.userRole === "User" ? (
                    <div className="btnContainer">
                    <Link to="/cart" style={{ color: "#226957", marginRight: 10 }}>
                        <ShoppingCartIcon/>
                    </Link>
                    <Link to="/myClass" style={{ color: "#226957", marginRight: 10, textDecoration: "none" }}>
                        My Class
                    </Link>
                    <Link to="/invoice" style={{ color: "#226957", marginRight: 10, textDecoration: "none" }}>
                        Invoice
                    </Link>
                    <div style={{ color: "#226957", marginRight: 10 }}>|</div>
                    <Link to="*" style={{ color: "#226957", marginRight: 10 }}>
                        <PersonIcon/>
                    </Link>
                    <Link to="/">
                        <Button onClick={() => logout()} variant="text" style={{ color: "red", marginRight: 10 }}><LogoutIcon /></Button>
                    </Link>
                </div>
                ) : (
                    <div className="btnContainer">
                        <Link to="/login">
                            <button className="btnLogin">Login</button>
                        </Link>
                        <Link to="/register">
                            <button className="btnRegister">Register</button>
                        </Link>
                    </div>
                )
            }
            </div>

            <Outlet/>
        </div>
    );
}