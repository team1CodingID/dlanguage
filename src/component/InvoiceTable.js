import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import { useState } from 'react';
import { useEffect } from 'react';
import useAuth from '../hook/useAuth';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function InvoiceTable() {
  const [order, setOrder] = useState([]);
  const { payload } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/order/invoice?customerId=${payload.customerId}`)
        .then(res => {
          setOrder(res.data);
          console.log("Data Accepted");
        })
        .catch(error => {
          console.error("Error fetching data:", error);
        });
  },[]);

  const details = (orderId) => {
    navigate('/invoice/Detail', {state : {orderId: orderId}});
  }
  
  return (
    <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
      <Table sx={{ maxWidth: 1400, mx: "auto" }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align='center'>No</StyledTableCell>
            <StyledTableCell align='center'>No. Invoice</StyledTableCell>
            <StyledTableCell align='center'>Date</StyledTableCell>
            <StyledTableCell align='center'>Total Course</StyledTableCell>
            <StyledTableCell align='center'>Total Price</StyledTableCell>
            <StyledTableCell align='center'>Action</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {order.map((row, index) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row" align='center'>
                {index+1}
              </StyledTableCell>
              <StyledTableCell align='center'>{row.invoiceCode}</StyledTableCell>
              <StyledTableCell align='center'>{row.orderDate}</StyledTableCell>
              <StyledTableCell align='center'>{row.totalCourse}</StyledTableCell>
              <StyledTableCell align='center'>{row.totalPrice}</StyledTableCell>
              <StyledTableCell align='center'>
                <Button onClick={() => details(row.orderId)} sx={{backgroundColor: "#EA9E1F", color: "white", width: 150, height: 40, borderRadius: 2, '&:hover': {backgroundColor: "#EA9E1F"}}}>Details</Button>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
