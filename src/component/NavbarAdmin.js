import '../component/Navbar.css';
import {Outlet, Link, useNavigate } from "react-router-dom";
import LogoutIcon from '@mui/icons-material/Logout';
import Button from '@mui/material/Button';  
import useAuth from "../hook/useAuth";
import Cookies from 'js-cookie';

export default function NavbarAdmin(){
    const { payload } = useAuth();
    const navigate = useNavigate();

    const logout = () => {
        Cookies.remove('payload');
        navigate('/');
        window.location.reload(true)
    }
    
    return(
        <div>
            <div className="container">
            <img className="logo" src={process.env.PUBLIC_URL + "/Logo.svg"} alt="Logo DLanguage"/>
            {payload && payload.userRole === "Admin" ? (
                <div className="btnContainer">
                    <Link to="/Admin" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Category
                    </Link>
                    <Link to="/Admin/course" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Course
                    </Link>
                    <Link to="/Admin/user" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        User
                    </Link>
                    <Link to="/Admin/method" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Method
                    </Link>
                    <Link to="/Admin/invoice" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Invoice
                    </Link>
                    <Link to="/Admin">
                        <Button onClick={() => logout()} variant="text" style={{ color: "red", marginRight: 20 }}><LogoutIcon /></Button>
                    </Link>
                </div>
            ) : (
                <div className="btnContainer">
                        <Link to="/login">
                            <button className="btnLogin">Login</button>
                        </Link>
                        <Link to="/register">
                            <button className="btnRegister">Register</button>
                        </Link>
                    </div>
            )}

            </div>

            <Outlet/>
        </div>
    );
}