import React, {useState, useEffect} from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Grid, CardActionArea } from '@mui/material';
import {useNavigate} from "react-router-dom";
import axios from "axios";

export default function Class() {
  const navigate = useNavigate();
  const [cls, setCls] = useState([]);

  useEffect(() => {
      axios.get(`${process.env.REACT_APP_API_URL}/class`).then((res) => {setCls(res.data); console.log(res.data);});
    }, []
  );

  const handleClass = (course, classId, courseId, imageData) => {
    navigate("/detailCourse/"+course, {state: { classId: classId, courseId: courseId, imageData: imageData }})
  }

  return (
    <div>
      <Grid container spacing={5} sx={{width: "fit-content", mx: 20, mt: 10}} xs={{mx: 0}}>
        {cls.map((course) => {
            return(
                <Grid item xs={12} md={6} lg={4} width={{xs:250}}>
                  <Card key={course.id} sx={{ height: 400, borderRadius: 10}}>
                    <CardActionArea onClick={() => handleClass(course.title, course.classId, course.courseId, course.imageData)}>
                      <CardMedia
                        component="img"
                        height="235"
                        image={`data:image/png;base64,${course.imageData}`}
                        alt={course.title}
                      />
                      <CardContent sx={{height: 150}}>
                        <Typography gutterBottom variant="body" component="div">
                          {course.courseName}
                        </Typography>
                        <Typography sx={{fontSize: 18, fontWeight: 600}}>
                          {course.title}
                        </Typography>
                        <Typography sx={{color: "#226957", fontSize: 18, fontWeight: 600, my: "auto", left: 2, bottom: 2}}>
                          IDR {course.price}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Grid>
              );
            }
          )
        }
      </Grid>
    </div>
  );
}