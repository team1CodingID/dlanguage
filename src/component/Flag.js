import React, {useState, useEffect} from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea, Grid } from '@mui/material';
import {useNavigate} from "react-router-dom";
import axios from "axios";

export default function Flag() {
  const navigate = useNavigate();
  const [cls, setCls] = useState([]);

  useEffect(() => {
      axios.get(`${process.env.REACT_APP_API_URL}/course`).then((res) => {setCls(res.data); console.log(res.data);});
    }, []
  );
  
  const handelClick = (courseName, courseId) =>{
    navigate(`/listClass/${courseName}`, { state: { courseId: courseId } })
  }

  return (
    <div>
      <Typography gutterBottom align='center' variant="h5" sx={{fontSize: 24, fontWeight: 600, color: "#226957"}}>
        Available Language Course
      </Typography>
      <Grid container spacing={2} sx={{width: "fit-content", mx: "auto", mt: 10}}>
        {cls.map(item => {
            return(
                <Grid item xs={6} lg={3}>
                  <Card key={item.courseId} sx={{ maxWidth: 300, mx:5 }}>
                    <CardActionArea onClick={() => handelClick(item.courseName, item.courseId)}>
                      <CardMedia
                        sx={{ height: 140, width: 300, borderRadius: 5, borderColor: "#BDBDBD" }}
                        component='img' 
                        image={`data:image/png;base64,${item.imageData}`}
                        title={item.courseName}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="div" align='center'>
                          {item.courseName}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Grid>
              );
            }
          )
        }
      </Grid>
    </div>
  );
}