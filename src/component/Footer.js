import { Grid, Typography } from "@mui/material";
import React from "react";
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';
import { Link } from "react-router-dom";

export default function Footer(){
    
    return(
        <div style={{backgroundColor: "#226957", }}>
            <Grid container spacing={2} 
            sx={
                {
                    width: "fit-content", 
                    mx: 13, 
                    mt:30,
                    color: "white",
                    paddingBottom: 5
                    }
                }>
                <Grid item xs={12} lg={4}>
                    <Typography fontSize={18} fontWeight={600}>
                        About Us
                    </Typography>
                    <Typography fontSize={16} fontWeight={200}>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                    </Typography>
                </Grid>
                <Grid item xs={12} lg={3}>
                    <Typography fontSize={18} fontWeight={600}>
                        Product
                    </Typography>
                    <Typography fontSize={16} fontWeight={200}>
                        <ul style={{margin: 0, paddingLeft: 20}}>
                            <Grid container rowSpacing={1} columnSpacing={1}>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/1">Arabic</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/2">Deutsch</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/3">English</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/4">French</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/5">Indonesian</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/6">Japanesee</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/7">Mandarin</Link></li>
                                </Grid>
                                <Grid item lg={6}>
                                    <li><Link style={{ textDecoration: "none", color: "white"}} to="/listClass/8">Melayu</Link></li>
                                </Grid>
                            </Grid>
                        </ul>
                    </Typography>
                </Grid>
                <Grid item xs={12} lg={5}>
                    <Typography fontSize={18} fontWeight={600}>
                        Address
                    </Typography>
                    <Typography fontSize={16} fontWeight={200}>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
                    </Typography>
                    <Typography fontSize={18} fontWeight={600}>
                        Contact Us
                    </Typography>
                    <div>
                        <LocalPhoneIcon sx={
                            {
                                backgroundColor: "white",
                                color: "#EA9E1F",
                                padding: 1,
                                borderRadius: 100
                            }
                        }/>
                        <InstagramIcon sx={
                            {
                                backgroundColor: "white",
                                color: "#EA9E1F",
                                padding: 1,
                                borderRadius: 100,
                                ml: 2
                            }
                        }/>
                        <YouTubeIcon sx={
                            {
                                backgroundColor: "white",
                                color: "#EA9E1F",
                                padding: 1,
                                borderRadius: 100,
                                ml: 2
                            }
                        }/>
                        <TelegramIcon sx={
                            {
                                backgroundColor: "white",
                                color: "#EA9E1F",
                                padding: 1,
                                borderRadius: 100,
                                ml: 2
                            }
                        }/>
                        <EmailIcon sx={
                            {
                                backgroundColor: "white",
                                color: "#EA9E1F",
                                padding: 1,
                                borderRadius: 100,
                                ml: 2
                            }
                        }/>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}