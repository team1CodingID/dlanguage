import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import PersonIcon from '@mui/icons-material/Person';
import { blue } from '@mui/material/colors';
import { Box } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import useAuth from '../hook/useAuth';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Payment(props) {
  const { onClose, open, listmethod } = props;
  const [method, setMethod] = useState("");
  const { payload } = useAuth();
  const navigate = useNavigate();

  const handleClose = () => {
    onClose(method);
    axios.put(`${process.env.REACT_APP_API_URL}/order?customerId=${payload.customerId}`)
    .then((res) => {console.log("Success With Response : "+res.data);navigate('/PaymentSuccess')})
    .catch(error => {console.error("Error with Response : "+error.response)});
  };

  const handleListItemClick = (value) => {
    setMethod(value);
    console.log(method);
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Select Payment Method</DialogTitle>
      <List sx={{ pt: 0 }}>
        {listmethod.map((item) => (
          <ListItem disableGutters key={item.methodId}>
            <ListItemButton onClick={() => handleListItemClick(item.methodName)}>
              <ListItemAvatar>
                <Avatar src={process.env.PUBLIC_URL+'/method/'+item.methodPath} sx={{ bgcolor: blue[100], color: blue[600] }} />
              </ListItemAvatar>
              <ListItemText primary={item.methodName} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleClose} sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Cancel</Button>
        <Button onClick={handleClose} sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Pay Now</Button>
      </Box>
    </Dialog>
  );
}

Payment.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

export default function PaymentMethod() {
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState("Gopay");
  const [listmethod, setList] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/AdminMethod/GetUser`)
    .then(res => {
      console.log("Succes Get Data");
      setList(res.data);
    })
    .catch(error => {
      console.error("Error with response : ", error.response.data);
    })
  },[]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{backgroundColor: "#226957", color: "white", width: 235, height: 40, borderRadius: 3, '&:hover': {backgroundColor: "#226957"}}}>Pay Now</Button>
      <Payment
        open={open}
        onClose={handleClose}
        listmethod={listmethod}
      />
    </div>
  );
}