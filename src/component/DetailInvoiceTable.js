import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import { useState } from 'react';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function DetailInvoiceTable() {
  const location = useLocation();
  const { orderId } = location.state;
  const [details, setDetails] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/order/detailInvoice?orderId=${orderId}`)
    .then(res => {
      setDetails(res.data);
      console.log("Success with Response: "+res);
    })
    .catch(error => {
      console.error("Error With Response: ", error);
    })
  }, [orderId])
  
  return (
    <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
      <Table sx={{ maxWidth: 1400, mx: "auto" }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align='center'>No</StyledTableCell>
            <StyledTableCell align='center'>Course Name</StyledTableCell>
            <StyledTableCell align='center'>Language</StyledTableCell>
            <StyledTableCell align='center'>Schedule</StyledTableCell>
            <StyledTableCell align='center'>Price</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {details.map((row, index) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row" align='center'>
                {index+1}
              </StyledTableCell>
              <StyledTableCell align='center'>{row.classTitle}</StyledTableCell>
              <StyledTableCell align='center'>{row.courseName}</StyledTableCell>
              <StyledTableCell align='center'>{row.scheduleDate}</StyledTableCell>
              <StyledTableCell align='center'>{row.price}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
