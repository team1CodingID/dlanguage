import React from "react";
import Flag from "../component/Flag.js";
import Footer from "../component/Footer.js";
import Class from "../component/Class.js";
import '../Landing Page/LandingPage.css'
import { Card, CardMedia, Grid, Typography } from "@mui/material";

export default function LandingPage(){

    return(
        <div>
            <div style={
                    {
                        backgroundImage: `url(${process.env.PUBLIC_URL + '/Landing1.svg'})`, 
                        height: 300,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: "column",
                        marginBottom: 50
                    }
                }>
                <Typography align="center" fontSize={{lg: 32, xs: 24}} width={{lg:600}} sx={
                        {
                            fontWeight: 700, 
                            color: "white",
                            mb: 2
                        }
                    }>
                    Learn different languages ​​to hone your communication skills
                </Typography>
                <Typography align="center" width={{lg: 1100}} color={"white"} fontSize={{lg: 24, xs: 16}}>
                    All the languages ​​you are looking for are available here, so what are you waiting for and immediately improve 
                    your language skills
                </Typography>
            </div>
            <div style={{marginLeft: "auto", marginRight: "auto", marginBottom: 50}}>
                <Grid container spacing= {2}>
                    <Grid item xs={4}>
                        <Typography sx={{textAlign: "center", color: "#226957", fontSize: 48, fontWeight: 600}}>
                            100+
                        </Typography>
                        <Typography  width={{lg: 260}}
                            sx={{textAlign: "center", fontSize: 16, fontWeight: 500, lineHeight: 2, mx: "auto"}}>
                            Choose the class you like and get the skills
                        </Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography sx={{textAlign: "center", color: "#226957", fontSize: 48, fontWeight: 600}}>
                            50+
                        </Typography>
                        <Typography width={{lg: 260}}
                            sx={{textAlign: "center", fontSize: 16, fontWeight: 500, lineHeight: 2, mx: "auto"}}>
                            Having teachers who are highly skilled and competent in the language
                        </Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography sx={{textAlign: "center", color: "#226957", fontSize: 48, fontWeight: 600}}>
                            10+
                        </Typography>
                        <Typography width={{lg: 260}}
                            sx={{textAlign: "center", fontSize: 16, fontWeight: 500, lineHeight: 2, mx: "auto"}}>
                            Many alumni become ministry employees because of their excellent language skills
                        </Typography>
                    </Grid>
                </Grid>
            </div>
            <Typography sx={{textAlign: "center", fontSize: 24, fontWeight: 600, color: "#226957"}}>
                Recommended Class
            </Typography>
            <Class/>
            <div className="benefit" style={{height: "fit-content", width: "fit-content", backgroundColor: "#EA9E1F", marginTop: 50, marginBottom: 50}}>
                <Grid container spacing= {2} sx={{pt: 10}} ml={{lg: 10, xs:0}} mr={{lg:10, xs:5}}>
                    <Grid item lg={8} xs={6}>
                        <Typography sx={{ fontSize: 32, fontWeight: 600, color: "white"}}>
                            Gets your best benefit
                        </Typography>
                        <Typography sx={{ fontSize: 16, fontWeight: 500, color: "white"}}>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam 
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
                            voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, 
                            sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                        </Typography>
                    </Grid>
                    <Grid item lg="auto" xs={6} sx={{maxWidth: 300}}>
                        <Card sx={{maxWidth: 300, backgroundColor: "transparent", borderRadius: 0}}>
                            <CardMedia
                                component="img"
                                height="235"
                                image={process.env.PUBLIC_URL+"Landing2.svg"}
                                alt="Person"
                            />
                        </Card>
                    </Grid>
                </Grid>
            </div>
            <Flag/>
            <Footer/>
        </div>
    );
}